#!/bin/bash
set -x
source secrets

oc create -f openshift/account.yml

oc process -f openshift/api.yml \
  -p S3_ACCESS_KEY_ID=${S3_ACCESS_KEY_ID} \
  -p S3_SECRET_ACCESS_KEY=${S3_SECRET_ACCESS_KEY} \
  -p S3_ENDPOINT=${S3_ENDPOINT} \
  -p S3_BUCKET=${S3_BUCKET} \
  -p S3_PREFIX=${S3_PREFIX} \
  -p SECRET_KEY=${SECRET_KEY} \
  | oc create -f -

oc process -f openshift/hello-world-build.yml | oc create -f -