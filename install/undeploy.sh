#!/bin/bash
#set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# delete API
oc process -f ${DIR}/templates/config.yml \
  -p S3_ENDPOINT="dontcareimdeleting" \
  -p S3_ACCESS_KEY_ID="dontcareimdeleting" \
  -p S3_SECRET_ACCESS_KEY="dontcareimdeleting" \
  -p S3_BUCKET="dontcareimdeleting" \
  | oc delete -f -

oc process -f ${DIR}/templates/hello-world-build.yml | oc delete -f -
oc process -f ${DIR}/templates/api.yml | oc delete -f -

# delete jobs
oc delete job -l app=jobs-api


