#!/bin/bash
#set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

REPLICAS=${API_REPLICAS:-1}
SOURCE_REPOSITORY_URL=${API_SOURCE_REPOSITORY_URL:-'https://gitlab.com/cfchase/jobs-api.git'}
SOURCE_REPOSITORY_REF=${API_SOURCE_REPOSITORY_REF:-'master'}

oc process -f ${DIR}/templates/config.yml \
  -p S3_ENDPOINT=${S3_ENDPOINT} \
  -p S3_REGION=${S3_REGION} \
  -p S3_ACCESS_KEY_ID=${S3_ACCESS_KEY_ID} \
  -p S3_SECRET_ACCESS_KEY=${S3_SECRET_ACCESS_KEY} \
  -p S3_BUCKET=${S3_BUCKET} \
  -p S3_PREFIX=${S3_PREFIX} \
  | oc create -f -

oc process -f ${DIR}/templates/hello-world-build.yml \
  | oc create -f -

oc process -f ${DIR}/templates/api.yml \
  -p REPLICAS=${REPLICAS} \
  -p SOURCE_REPOSITORY_URL=${SOURCE_REPOSITORY_URL} \
  -p SOURCE_REPOSITORY_REF=${SOURCE_REPOSITORY_REF} \
  | oc create -f -


