#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


ENV_FILE=${DIR}/.env
source ${ENV_FILE}
for i in $(sed 's/=.*//' ${ENV_FILE}); do export "$i"; done

export S3_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export S3_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export S3_ENDPOINT=$S3_ENDPOINT
export S3_BUCKET=$S3_BUCKET
export S3_PREFIX=$S3_PREFIX

export JOB_CPU_REQUEST=$JOB_CPU_REQUEST
export JOB_MEMORY_REQUEST=$JOB_MEMORY_REQUEST
export JOB_CPU_LIMIT=$JOB_CPU_LIMIT
export JOB_MEMORY_LIMIT=$JOB_MEMORY_LIMIT

export SECRET_KEY="dev"
export SERVER_PORT=$SERVER_PORT
export OFF_CLUSTER=True

export FLASK_DEBUG=true

CMD="${@:-runserver}"
echo ${CMD}

cd ${DIR}/api
pwd
pipenv sync
pipenv run python wsgi.py
