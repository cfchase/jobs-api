ENV_FILE := .env
include ${ENV_FILE}
export $(shell sed 's/=.*//' ${ENV_FILE})

.DEFAULT_GOAL := deploy

.PHONY: oc-login
oc-login:
	${OC} login ${OC_URL} -u ${OC_USER} -p ${OC_PASSWORD} --insecure-skip-tls-verify=true
	${OC} project ${PROJECT} 2> /dev/null || oc new-project ${PROJECT}

.PHONY: api
api: oc-login
	./install/deploy-api.sh

.PHONY: deploy
deploy: oc-login
	./install/deploy.sh

.PHONY: undeploy
undeploy: oc-login
	./install/undeploy.sh


