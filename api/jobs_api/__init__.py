import os

from flask import Flask
from jobs_api.config import Config


def create_app(app_name):
    jobs_api = Flask(app_name)

    jobs_api.config.from_object(Config)

    from jobs_api.apiv1 import apiv1
    jobs_api.register_blueprint(apiv1, url_prefix='/api/v1')

    return jobs_api
